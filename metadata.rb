name             "s3-layer"
maintainer       "awschef"
license          "All rights reserved"
description      "Download file from s3 bucket and install file on isntance"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.0.1"

depends "aws", "= 2.5.0"

